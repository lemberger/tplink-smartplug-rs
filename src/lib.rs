use serde::Deserialize;
use socket2::{Domain, Socket, Type};
use std::clone::Clone;
use std::cmp::Eq;
use std::cmp::PartialEq;
use std::net::SocketAddr;
use std::str::FromStr;

const PORT: u32 = 9999;

#[derive(Clone, Deserialize, Debug)]
pub struct Plug {
    pub name: String,
    ip: String,
    port: u32,
}

impl Plug {
    pub fn new(name: &str, ip: &str, port: Option<u32>) -> Self {
        let act_port = match port {
            Some(p) => p,
            None => PORT,
        };
        Plug {
            name: name.to_string(),
            ip: ip.to_string(),
            port: act_port,
        }
    }

    pub fn send(&self, cmd: Command) -> Result<String, Box<dyn std::error::Error + 'static>> {
        let socket = self.connect()?;
        socket.send(&cmd.get_message())?;
        let mut rcv_buf = [0; 2048];
        let size = socket.recv(&mut rcv_buf)?;
        Ok(decrypt(&rcv_buf[..size]))
    }

    fn connect(&self) -> Result<Socket, Box<dyn std::error::Error + 'static>> {
        let addr = &format!("{}:{}", self.ip, self.port)
            .parse::<SocketAddr>()?
            .into();
        let socket = Socket::new(Domain::ipv4(), Type::stream(), None)?;
        socket.connect(addr)?;
        Ok(socket)
    }
}

pub enum Command {
    Info,
    On,
    Off,
    CloudInfo,
    WlanScan,
    Time,
    Schedule,
    Countdown,
    AntiTheft,
    Reboot,
    Reset,
    Energy,
}

impl Command {
    pub fn get_message(&self) -> Vec<u8> {
        Self::encrypt(self.get_command())
    }

    fn get_command(&self) -> &str {
        match self {
            Command::Info => "{\"system\":{\"get_sysinfo\":{}}}",
            Command::On => "{\"system\":{\"set_relay_state\":{\"state\":1}}}",
            Command::Off => "{\"system\":{\"set_relay_state\":{\"state\":0}}}",
            Command::CloudInfo => "{\"cnCloud\":{\"get_info\":{}}}",
            Command::WlanScan => "{\"netif\":{\"get_scaninfo\":{\"refresh\":0}}}",
            Command::Time => "{\"time\":{\"get_time\":{}}}",
            Command::Schedule => "{\"schedule\":{\"get_rules\":{}}}",
            Command::Countdown => "{\"count_down\":{\"get_rules\":{}}}",
            Command::AntiTheft => "{\"anti_theft\":{\"get_rules\":{}}}",
            Command::Reboot => "{\"system\":{\"reboot\":{\"delay\":1}}}",
            Command::Reset => "{\"system\":{\"reset\":{\"delay\":1}}}",
            Command::Energy => "{\"emeter\":{\"get_realtime\":{}}}",
        }
    }

    fn encrypt(msg: &str) -> Vec<u8> {
        let mut key = 171;
        let mut result = (msg.chars().count() as u32).to_be_bytes().to_vec();
        for c in msg.chars() {
            let a = key ^ (c as u8);
            key = a;
            result.push(a.to_be());
        }
        result
    }
}

pub fn decrypt(msg: &[u8]) -> String {
    let mut key = 171;
    let mut result = String::new();
    for c in &msg[4..] {
        let a = key ^ *c;
        result.push(a as char);
        key = *c;
    }
    result
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
pub struct EnergyMeasure {
    pub current_ma: u32,
    pub power_mw: u32,
    pub total_wh: u32,
    pub voltage_mv: u32,
    pub err_code: u8,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
pub struct Emeter {
    pub get_realtime: EnergyMeasure,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
pub struct EnergyResponse {
    pub emeter: Emeter,
}

impl FromStr for EnergyResponse {
    type Err = serde_json::error::Error;

    fn from_str(msg: &str) -> Result<Self, Self::Err> {
        serde_json::from_str(msg)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn command_to_str() {
        let cmd = Command::Info;
        let cmd_str = cmd.get_command();
        assert_eq!(cmd_str, "{\"system\":{\"get_sysinfo\":{}}}");
    }

    #[test]
    fn encrypt_cmd_info() {
        let cmd = Command::Info;
        let expected_msg: Vec<u8> = vec![
            0, 0, 0, 29, 208, 242, 129, 248, 139, 255, 154, 247, 213, 239, 148, 182, 209, 180, 192,
            159, 236, 149, 230, 143, 225, 135, 232, 202, 240, 139, 246, 139, 246,
        ];

        compare_encrypt_and_vec(cmd, expected_msg);
    }

    #[test]
    fn encrypt_cmd_on() {
        let cmd = Command::On;
        let expected_msg: Vec<u8> = vec![
            0, 0, 0, 42, 208, 242, 129, 248, 139, 255, 154, 247, 213, 239, 148, 182, 197, 160, 212,
            139, 249, 156, 240, 145, 232, 183, 196, 176, 209, 165, 192, 226, 216, 163, 129, 242,
            134, 231, 147, 246, 212, 238, 223, 162, 223, 162,
        ];

        compare_encrypt_and_vec(cmd, expected_msg);
    }

    #[test]
    fn encrypt_cmd_off() {
        let cmd = Command::Off;
        let expected_msg: Vec<u8> = vec![
            0, 0, 0, 42, 208, 242, 129, 248, 139, 255, 154, 247, 213, 239, 148, 182, 197, 160, 212,
            139, 249, 156, 240, 145, 232, 183, 196, 176, 209, 165, 192, 226, 216, 163, 129, 242,
            134, 231, 147, 246, 212, 238, 222, 163, 222, 163,
        ];

        compare_encrypt_and_vec(cmd, expected_msg);
    }

    #[test]
    fn encrypt_cmd_energy() {
        let cmd = Command::Energy;
        let expected_msg: Vec<u8> = vec![
            0, 0, 0, 30, 208, 242, 151, 250, 159, 235, 142, 252, 222, 228, 159, 189, 218, 191, 203,
            148, 230, 131, 226, 142, 250, 147, 254, 155, 185, 131, 248, 133, 248, 133,
        ];

        compare_encrypt_and_vec(cmd, expected_msg);
    }

    fn compare_encrypt_and_vec(cmd: Command, expected: Vec<u8>) {
        let message = Command::encrypt(cmd.get_command());

        assert_eq!(
            message.len(),
            expected.len(),
            "Length of vectors differs: {:?} vs. {:?}",
            message,
            expected
        );
        assert_eq!(
            message, expected,
            "Content of vectors differs: {:?} vs. {:?}",
            message, expected
        );
    }

    #[test]
    fn decrypt_response_info() {
        let msg: Vec<u8> = vec![
            0, 0, 2, 107, 208, 242, 129, 248, 139, 255, 154, 247, 213, 239, 148, 182, 209, 180,
            192, 159, 236, 149, 230, 143, 225, 135, 232, 202, 240, 139, 169, 218, 173, 242, 132,
            225, 147, 177, 139, 169, 152, 182, 131, 173, 153, 185, 251, 142, 231, 139, 239, 207,
            254, 198, 246, 206, 255, 202, 234, 184, 221, 177, 159, 174, 156, 173, 153, 173, 157,
            191, 147, 177, 217, 174, 241, 135, 226, 144, 178, 136, 170, 152, 182, 134, 164, 136,
            170, 222, 167, 215, 178, 144, 170, 136, 193, 142, 218, 244, 167, 234, 171, 249, 173,
            253, 177, 228, 163, 240, 167, 238, 186, 249, 177, 147, 191, 157, 240, 159, 251, 158,
            242, 208, 234, 200, 128, 211, 226, 211, 227, 203, 142, 219, 242, 208, 252, 222, 179,
            210, 177, 147, 169, 139, 207, 247, 205, 253, 185, 131, 178, 133, 191, 254, 186, 128,
            196, 243, 201, 255, 198, 228, 200, 234, 142, 235, 157, 194, 172, 205, 160, 197, 231,
            221, 255, 172, 193, 160, 210, 166, 134, 209, 184, 149, 211, 186, 154, 202, 166, 211,
            180, 148, 195, 170, 222, 182, 150, 211, 189, 216, 170, 205, 180, 148, 217, 182, 216,
            177, 197, 170, 216, 177, 223, 184, 154, 182, 148, 245, 153, 240, 145, 226, 192, 250,
            216, 139, 232, 128, 242, 151, 254, 156, 232, 129, 242, 145, 249, 219, 247, 213, 167,
            194, 174, 207, 182, 233, 154, 238, 143, 251, 158, 188, 134, 183, 155, 185, 214, 184,
            231, 147, 250, 151, 242, 208, 234, 223, 236, 212, 227, 207, 237, 140, 239, 155, 242,
            132, 225, 190, 211, 188, 216, 189, 159, 165, 135, 233, 134, 232, 141, 175, 131, 161,
            199, 162, 195, 183, 194, 176, 213, 247, 205, 239, 187, 242, 191, 133, 192, 142, 203,
            233, 197, 231, 146, 226, 134, 231, 147, 250, 148, 243, 209, 235, 219, 247, 213, 188,
            223, 176, 222, 129, 233, 136, 251, 147, 177, 139, 169, 139, 167, 133, 247, 132, 247,
            158, 188, 134, 171, 158, 169, 133, 167, 203, 174, 202, 149, 250, 156, 250, 216, 226,
            210, 254, 220, 176, 223, 177, 214, 191, 203, 190, 218, 191, 224, 137, 171, 145, 160,
            147, 167, 145, 165, 149, 185, 155, 247, 150, 226, 139, 255, 138, 238, 139, 212, 189,
            159, 165, 145, 169, 156, 171, 152, 171, 135, 165, 205, 186, 243, 151, 181, 143, 173,
            157, 169, 157, 220, 233, 216, 238, 171, 238, 216, 235, 168, 144, 167, 146, 212, 237,
            217, 236, 212, 144, 209, 227, 214, 149, 167, 228, 167, 228, 209, 144, 160, 130, 174,
            140, 234, 157, 212, 176, 146, 168, 138, 186, 138, 186, 138, 186, 138, 186, 138, 186,
            138, 186, 138, 186, 138, 186, 138, 186, 138, 186, 138, 186, 138, 186, 138, 186, 138,
            186, 138, 186, 138, 186, 138, 168, 132, 166, 194, 167, 209, 184, 219, 190, 247, 147,
            177, 139, 169, 145, 161, 145, 167, 225, 160, 152, 175, 234, 216, 233, 221, 236, 169,
            157, 223, 231, 211, 234, 175, 159, 175, 158, 218, 233, 175, 233, 219, 226, 161, 224,
            161, 144, 210, 227, 162, 231, 211, 228, 211, 241, 221, 255, 144, 245, 152, 209, 181,
            151, 173, 143, 190, 135, 190, 134, 199, 246, 194, 134, 199, 134, 190, 136, 205, 249,
            188, 140, 188, 141, 203, 143, 184, 251, 186, 252, 200, 250, 194, 244, 204, 142, 187,
            254, 220, 240, 210, 188, 217, 161, 213, 138, 235, 136, 252, 149, 250, 148, 182, 140,
            247, 213, 161, 216, 168, 205, 239, 213, 248, 201, 180, 152, 186, 223, 173, 223, 128,
            227, 140, 232, 141, 175, 149, 165, 216, 165, 216,
        ];
        let expected = "{\"system\":{\"get_sysinfo\":{\"sw_ver\":\"1.5.4 Build 180815 Rel.121440\",\"hw_ver\":\"2.0\",\"type\":\"IOT.SMARTPLUGSWITCH\",\"model\":\"HS110(EU)\",\"mac\":\"D8:0D:17:AD:D7:69\",\"dev_name\":\"Smart Wi-Fi Plug With Energy Monitoring\",\"alias\":\"Schreibtisch\",\"relay_state\":1,\"on_time\":5387,\"active_mode\":\"none\",\"feature\":\"TIM:ENE\",\"updating\":0,\"icon_hash\":\"\",\"rssi\":-57,\"led_off\":0,\"longitude_i\":134640,\"latitude_i\":485733,\"hwId\":\"044A516EE63C875F9458DA25C2CCC5A0\",\"fwId\":\"00000000000000000000000000000000\",\"deviceId\":\"8006FA87E2141E4B849E001D3FF29CAA1B1AE477\",\"oemId\":\"1998A14DAA86E4E001FD7CAF42868B5E\",\"next_action\":{\"type\":-1},\"err_code\":0}}}";

        let response = decrypt(&msg);

        assert_eq!(response, expected);
    }

    #[test]
    fn decrypt_response_error() {
        let msg: Vec<u8> = vec![
            0, 0, 0, 45, 208, 242, 129, 248, 139, 255, 154, 247, 213, 239, 148, 182, 197, 160, 212,
            139, 249, 156, 240, 145, 232, 183, 196, 176, 209, 165, 192, 226, 216, 163, 129, 228,
            150, 228, 187, 216, 183, 211, 182, 148, 174, 158, 227, 158, 227,
        ];
        let expected = "{\"system\":{\"set_relay_state\":{\"err_code\":0}}}";

        let response = decrypt(&msg);

        assert_eq!(response, expected);
    }

    #[test]
    fn decrypt_encrypt() {
        let msg = "Hello World";

        let result = decrypt(&Command::encrypt(msg));

        assert_eq!(result, msg);
    }

    #[test]
    fn decrypt_energy_response() {
        let msg: Vec<u8> = vec![
            0, 0, 0, 112, 208, 242, 151, 250, 159, 235, 142, 252, 222, 228, 159, 189, 218, 191,
            203, 148, 230, 131, 226, 142, 250, 147, 254, 155, 185, 131, 248, 218, 172, 195, 175,
            219, 186, 221, 184, 231, 138, 252, 222, 228, 214, 228, 211, 228, 211, 224, 204, 238,
            141, 248, 138, 248, 157, 243, 135, 216, 181, 212, 246, 204, 254, 199, 246, 218, 248,
            136, 231, 144, 245, 135, 216, 181, 194, 224, 218, 238, 220, 235, 221, 238, 194, 224,
            148, 251, 143, 238, 130, 221, 170, 194, 224, 218, 237, 218, 226, 208, 252, 222, 187,
            201, 187, 228, 135, 232, 140, 233, 203, 241, 193, 188, 193, 188,
        ];
        let expected = EnergyResponse {
            emeter: Emeter {
                get_realtime: EnergyMeasure {
                    current_ma: 291,
                    power_mw: 42763,
                    total_wh: 7782,
                    voltage_mv: 227773,
                    err_code: 0,
                },
            },
        };

        let result = EnergyResponse::from_str(&decrypt(&msg)).unwrap();

        assert_eq!(result, expected);
    }
}
