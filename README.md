# tplink-smartplug-rs

A rust library for interaction with TP-Link smartplugs.
Allows querying info, status, and energy measurements,
as well as turning the plugs on and off.

## Building

Build the library with

```shell
cargo build
```

To make sure that the library was compiled successfully, run:

```shell
cargo test
```
